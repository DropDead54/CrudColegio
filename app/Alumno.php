<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $table = 'alumnos';

    protected $fillable = [
        'nombre','apellido','genero','fecha_nacimiento'
    ];

    public function grados(){
        return $this->belongsToMany('App\Grado');
    }
    
}
