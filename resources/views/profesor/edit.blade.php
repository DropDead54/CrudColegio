@extends('template.main')

@section('title','Editar Profesor' . $profesor->nombre) 

@section('content')
    {!! Form::open(['route' => ['profesor.update',$profesor->id], 'method' => 'PUT']) !!}
        
    <div class="form-group">
        {!! Form::label('nombre','Nombre') !!}
        {!! Form::text('nombre',$profesor->nombre ,[ 'class'=>'form-control', 'placeholder' => 'Nombre Completo', 'required' ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('apellido','Apellido ') !!}
        {!! Form::text('apellido',$profesor->apellido ,[ 'class'=>'form-control', 'placeholder' => 'Apellido Completo', 'required' ]) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('genero','Genero') !!}
        {!! Form::select('genero',['Hombre'=>'Hombre', 'Mujer'=>'Mujer'],$profesor->genero,[ 'class'=>'form-control', 'placeholder' => 'Selecciona una opción', 'required' ]) !!}
    </div>

    
    <div class="form-group">
        {!! Form::submit('Editar Profesor', [ 'class'=>'btn btn-success']) !!}
    </div>

    {!! Form::close() !!}
@endsection