@extends('template.main')

@section('title','Grados y Alumnos') 

@section('content')
<br><a href="{{route('grado_alumno.create')}}" class="btn btn-success"> Asignar grado a Alumno</a><br><br>
<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Seccion</th>
                <th scope="col">Alumno</th>
                <th scope="col">Grado</th>
            </tr>
        </thead>
        <tbody>
        
                @foreach($gradosAlumno as $gradoAlumno)
                <tr>
                    <th scope="row">{{$gradoAlumno->id}}</th>
                    <td>{{$gradoAlumno->seccion}}</td>
                   
                    @foreach($alumnos as $alumno)
                        @if($alumno->id == $gradoAlumno->alumno_id )
                            <td>{{$alumno->nombre}}</td>
                        @endif
                    @endforeach


                    @foreach($grados as $grado)
                        @if($grado->id == $gradoAlumno->grado_id )
                            <td>{{$grado->nombre}}</td>
                        @endif
                    @endforeach

                    <td>
                        
                        {!! Form::open(['route' => ['grado_alumno.destroy',$gradoAlumno->id], 'method' => 'DELETE']) !!}
                            <a href="{{route('grado_alumno.edit',$gradoAlumno->id)}}" class="btn btn-danger"><i class="fas fa-pencil-alt"></i></a> 
                            <button type="submit" class="btn btn-warning" onclick="return confirm('Seguro que quieres elimanar {{$gradoAlumno->nombre}} ?')"><i class="fas fa-trash-alt"></i></button>
                        {!! Form::close() !!}
                        
                    </td>
                </tr>
                @endforeach
        </tbody>
    </table>
</div>
<div class="text-center">
    {!! $gradosAlumno->render() !!}
</div>

@endsection