<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal"><a style="text-decoration:none; color:black" href="/"><i class="fas fa-graduation-cap"></i> Colegio</h5></a>
    <nav class="my-2 my-md-0 mr-md-3">
      <a class="p-2 text-dark" href="{{route('alumno.index')}}">Alumnos</a>
      <a class="p-2 text-dark" href="{{route('profesor.index')}}">Profesores</a>
      <a class="p-2 text-dark" href="{{route('grado.index')}}">Grados</a>
      <a class="p-2 text-dark" href="{{route('grado_alumno.index')}}">Grados y Alumnos</a>
    </nav>
  </div>