<footer class="pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
        <div class="col-12 col-md">
            <i class="fas fa-edit"></i>
            <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
        </div>
        <div class="col-6 col-md" style="text-align:right">
            <h3>Administracion del colegio</h3>
            <nav class="my-2 my-md-0 mr-md-3">
                <a class="text-muted p-2 text-dark" href="{{route('alumno.index')}}">Alumnos</a>
                <a class="text-muted p-2 text-dark" href="{{route('profesor.index')}}">Profesores</a>
                <a class="text-muted p-2 text-dark" href="{{route('grado.index')}}">Grados</a>
                <a class="text-muted p-2 text-dark" href="{{route('grado_alumno.index')}}">Grados y Alumnos</a>
            </nav>
        </div>
        
    </div>
</footer>